﻿using Bunit;

namespace Blazter.Overlay.Test.Unit.Core
{
	public class OverlayContainerTest
	{
		private IRenderedComponent<OverlayContainer> CreateCUT(bool visible)
		{
			var context = new TestContext();


			return context.RenderComponent<OverlayContainer>(builder => builder.Add(c => c.Visible, visible));
		}

		[Fact(DisplayName = "[UNIT][OVC-001] - Hide Overlay")]
		public void OverlayContainer_HideOverlay()
		{
			// Arrange
			var cut = CreateCUT(false);

			// Act

			// Assert
			Assert.Empty(cut.FindAll(Selectors.ByTestId("cntOverlay")));
		}

		[Fact(DisplayName = "[UNIT][OVC-002] - Show Overlay")]
		public void OverlayContainer_ShowOverlay()
		{
			// Arrange
			var cut = CreateCUT(true);

			// Act

			// Assert
			Assert.Single(cut.FindAll(Selectors.ByTestId("cntOverlay")));
		}
	}
}
