﻿using Blazter.Overlay.Loading.Services;

namespace Blazter.Overlay.Test.Unit.Loading
{
	public class LoadingServiceTest
	{
		private LoadingService CreateSUT()
		{
			return new LoadingService();
		}

		[Fact(DisplayName = "[UNIT][LDS-001] - Show Loading Screen")]
		public void LoadingService_Show_ShowLoadingScreen()
		{
			// Arrange
			var sut = CreateSUT();

			// Act
			sut.Show();

			// Assert
			Assert.True(sut.Visible);
		}

		[Fact(DisplayName = "[UNIT][LDS-002] - Raise Changed Event on Show")]
		public void LoadingService_Show_RaiseChangedEventOnShow()
		{
			// Arrange
			var sut = CreateSUT();

			// Act
			// Assert
			Assert.Raises<EventArgs>(h => sut.Changed += h, h => sut.Changed -= h, () => sut.Show());
		}

		[Fact(DisplayName = "[UNIT][LDS-003] - Close Loading Screen")]
		public void LoadingService_Close_Close()
		{
			// Arrange
			var sut = CreateSUT();

			sut.Show();

			// Act
			sut.Close();

			// Assert
			Assert.False(sut.Visible);
		}

		[Fact(DisplayName = "[UNIT][LDS-004] - Raise Changed Event on Close")]
		public void LoadingService_Close_RaiseChangedEventOnClose()
		{
			// Arrange
			var sut = CreateSUT();

			sut.Show();

			// Act
			// Assert
			Assert.Raises<EventArgs>(h => sut.Changed += h, h => sut.Changed -= h, sut.Close);
		}

		[Fact(DisplayName = "[UNIT][LDS-005] - Close Multiple Times Shown Loading Screen")]
		public void LoadingService_Close_CloseMultipleTimesShownLoadingScreen()
		{
			// Arrange
			var sut = CreateSUT();

			sut.Show();
			sut.Show();

			// Act
			sut.Close();

			// Assert
			Assert.True(sut.Visible);
		}

		[Fact(DisplayName = "[UNIT][LDS-006] - Close on Dispose")]
		public void LoadingService_Close_CloseOnDispose()
		{
			// Arrange
			var sut = CreateSUT();

			var loading = sut.Show();

			// Act
			loading.Dispose();

			// Assert
			Assert.False(sut.Visible);
		}
	}
}
