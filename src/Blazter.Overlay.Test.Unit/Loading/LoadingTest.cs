﻿using Blazter.Overlay.Loading.Services;
using Bunit;
using Microsoft.Extensions.DependencyInjection;

namespace Blazter.Overlay.Test.Unit.Loading
{
	public class LoadingTest
	{
		private LoadingService _loadingService = null!;

		private IRenderedComponent<LoadingScreen> CreateCUT()
		{
			_loadingService = new LoadingService();

			var context = new TestContext();
			context.Services.AddSingleton(_loadingService);

			return context.RenderComponent<LoadingScreen>();
		}

		[Fact(DisplayName = "[UNIT][LOD-001] - Show Loading")]
		public void Loading_ShowLoading()
		{
			// Arrange
			var cut = CreateCUT();

			// Act
			cut.InvokeAsync(() => _loadingService.Show());

			// Assert
			Assert.NotEmpty(cut.FindAll(Selectors.ByTestId("cntLoading")));
		}

		[Fact(DisplayName = "[UNIT][LOD-002] - Hide Loading")]
		public void Loading_HideLoading()
		{
			// Arrange
			var cut = CreateCUT();

			cut.InvokeAsync(() => _loadingService.Show());

			// Act
			cut.InvokeAsync(() => _loadingService.Close());

			// Assert
			Assert.Empty(cut.FindAll(Selectors.ByTestId("cntLoading")));
		}
	}
}
