﻿using Blazter.Overlay.Loading.Services;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace Blazter
{
	public static class WebAssemblyHostBuilderExtensions
	{
		public static WebAssemblyHostBuilder AddLoading(this WebAssemblyHostBuilder builder)
		{
			builder.Services.AddSingleton(_ => new LoadingService());
			builder.Services.AddTransient<ILoadingService>(provider => provider.GetRequiredService<LoadingService>());

			return builder;
		}
	}
}
