﻿
namespace Blazter.Overlay.Loading.Services
{
	public class LoadingService : ILoadingService
	{
		private int _count;

		private bool _visible;
		public bool Visible { get { return _visible; } private set { _visible = value; Changed?.Invoke(this, EventArgs.Empty); } }

		public event EventHandler<EventArgs>? Changed;

		public IDisposable Show()
		{
			if (!Visible) Visible = true;
			_count++;

			return this;
		}

		public void Close()
		{
			if (--_count == 0) Visible = false;
		}

#pragma warning disable CA1816 // Dispose methods should call SuppressFinalize
		void IDisposable.Dispose()
#pragma warning restore CA1816 // Dispose methods should call SuppressFinalize
		{
			Close();
		}
	}
}
