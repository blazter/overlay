﻿namespace Blazter
{
	public interface ILoadingService : IDisposable
	{
		IDisposable Show();
		void Close();
	}
}
